
# base image
FROM python:3.11-rc-bullseye

# create a virtual environment to hold pip packages
ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# copy the application folder
COPY . .

# install packages listed in requirements file
RUN pip install -r requirements.txt

# working image directory
#WORKDIR flask-app

EXPOSE 8000

# run flask application
CMD exec gunicorn --bind 0.0.0.0:8000 wsgi:app
